package com.ricardocastilla.rockpaperspear;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.view.View;
import android.util.Log;
import android.app.AlertDialog;
import android.media.MediaPlayer;
import android.widget.TextView;

import java.util.Random;


public class MainActivity extends ActionBarActivity {

    private int pscore = 0; // player score
    private int cscore = 0; // computer score

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set view for play button
        Button playBtn;
        playBtn = (Button) findViewById(R.id.playBtn);
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("click", "Clicked!");
                checkWinner(view);
            } // onClick
        });

    } // onCreate

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean checkWinner(View view) {

        MediaPlayer rockSound = MediaPlayer.create(getApplicationContext(), R.raw.rockfallstrimmed);
        MediaPlayer paperSound = MediaPlayer.create(getApplicationContext(), R.raw.papercrumple);
        MediaPlayer spearSound = MediaPlayer.create(getApplicationContext(), R.raw.spearthrow);

        final String ROCK_STR = "Rock"; // 0
        final String PAPER_STR = "Paper"; // 1
        final String SPEAR_STR = "Spear"; // 2
        final int ROCK_INT = 0; // 0
        final int PAPER_INT = 1; // 1
        final int SPEAR_INT = 2; // 2

        RadioGroup radioChoiceGroup;
        RadioButton radioChoiceButton;

        // obtain choice from the computer
        int computerChoice = selectChoice_computer(view);
        // initialize in for player
        int playerChoice;

        radioChoiceGroup = (RadioGroup) findViewById(R.id.Choices);

        // get the id of the checked radio
        int choiceId = radioChoiceGroup.getCheckedRadioButtonId();
        Log.d("choice", "ChoiceID: " + choiceId);
        // make sure a choice was made
        if ( -1 != choiceId ) {

            TextView player_score = (TextView) findViewById(R.id.scorePlayer);
            TextView computer_score = (TextView) findViewById(R.id.scoreComputer);

            radioChoiceButton = (RadioButton) findViewById(choiceId); // get the view of the button

            String choiceStr = (String) radioChoiceButton.getText();

            // assign an int value to player choice
            if ( SPEAR_STR.equals(choiceStr) ) {
                playerChoice = SPEAR_INT;
                spearSound.start();
            } // if spear
            else if ( ROCK_STR.equals(choiceStr)) {
                playerChoice = ROCK_INT;
                rockSound.start();
            } // else-if rock
            else {
                playerChoice = PAPER_INT;
                paperSound.start();
            } // else paper

            // compare the values
            if ( ROCK_INT == playerChoice ) {
                if ( PAPER_INT == computerChoice ) {
                    new AlertDialog.Builder(this).setTitle("Computer wins!")
                            .setMessage("The paper has taken over the rock!")
                            .setNeutralButton("Close", null)
                            .show();
                    computer_score.setText("Computer Score: " + ++cscore);
                } // if

                else if ( SPEAR_INT == computerChoice ) {
                    new AlertDialog.Builder(this).setTitle("Player wins!")
                            .setMessage("The rock broke the spear!")
                            .setNeutralButton("Close", null)
                            .show();
                    player_score.setText("Player Score: " + ++pscore); // update score
                } // else if
                else {
                    new AlertDialog.Builder(this).setTitle("Draw!")
                            .setMessage("There was a draw!")
                            .setNeutralButton("Close", null)
                            .show();
                }
                Log.d("Selection", "Player chose Rock");
            } // if
            else if ( PAPER_INT == playerChoice ) {
                if ( ROCK_INT == computerChoice ) {
                    new AlertDialog.Builder(this).setTitle("Player wins!")
                            .setMessage("The paper has taken over the rock!")
                            .setNeutralButton("Close", null)
                            .show();
                    player_score.setText("Player Score: " + ++pscore); // update score
                } // if
                else if ( SPEAR_INT == computerChoice ) {
                    new AlertDialog.Builder(this).setTitle("Computer wins!")
                            .setMessage("The spear goes through the paper!")
                            .setNeutralButton("Close", null)
                            .show();
                    computer_score.setText("Computer Score: " + ++cscore); // update score
                } // else if
                else {
                    new AlertDialog.Builder(this).setTitle("Draw!")
                            .setMessage("There was a draw!")
                            .setNeutralButton("Close", null)
                            .show();
                }
                Log.d("Selection", "Player chose Paper");
            } // else-if
            // else, it's spear
            else {
                if ( ROCK_INT == computerChoice ) {
                    new AlertDialog.Builder(this).setTitle("Computer wins!")
                            .setMessage("The rock breaks the spear!")
                            .setNeutralButton("Close", null)
                            .show();
                    computer_score.setText("Computer Score: " + ++cscore);
                } // if
                else if ( PAPER_INT == computerChoice ) {
                    new AlertDialog.Builder(this).setTitle("Player wins!")
                            .setMessage("The spear goes through the paper!")
                            .setNeutralButton("Close", null)
                            .show();
                    player_score.setText("Player Score: " + ++pscore); // update score
                } // else if
                else {
                    new AlertDialog.Builder(this).setTitle("Draw!")
                            .setMessage("There was a draw!")
                            .setNeutralButton("Close", null)
                            .show();
                }
                Log.d("Selection", "Player chose Spear");
            } // else
            return true;
        } // if a choice was checked
        // no radio selected
        else {
            new AlertDialog.Builder(this).setTitle("No Selection")
                    .setMessage("You must make a selection!")
                    .setNeutralButton("Close", null)
                    .show();
        } // else
        return false;
    } // checkWinner

    public int selectChoice_computer(View view) {
        Log.d("selection", "Computer making a choice");
        int max = 3;
        int min = 0;
        Random rand = new Random(); // create random number generator

        int randNum = rand.nextInt(max - min) + min; // save the random number
        Log.d("selection", "computer chose: " + randNum);

        return randNum;
    } // selectChoice_computer
} // MainActivity
