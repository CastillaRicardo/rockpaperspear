# Rock Paper Spear #

Rock Paper Scissor game for Android with an FSU Theme

Requirements:

* Ram: Any
* Disk Space: 1.1 MB for download, about 1.5MB after installation
* Android version: 2.2.x and above required

Tools used:

* Built on Android Studio
* Compile SDK Version: 21
* Build Tools Version: 22.0.1